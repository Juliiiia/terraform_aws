variable "bucket" {
  type        = string
  description = "Name bucket" 
}

variable "acl" {
  type        = string
  description = "The canned ACL to apply. Defaults to private" 
}

variable "versioning" {
  type        = map(string)
  description = "Map containing versioning configuration" 
}

variable "force_destroy" {
  type        = string
  description = "A boolean that indicates all objects should be deleted from the bucket so that the bucket can be destroyed without error. These objects are not recoverable" 
}
