module "s3_bucket" {
  source            = "terraform-aws-modules/s3-bucket/aws"
  bucket            = var.bucket
  acl               = var.acl
  versioning        = var.versioning
  force_destroy     = var.force_destroy


}