
variable "identifier" {
  type        = string
  description = "The name of the RDS instance, if omitted, Terraform will assign a random, unique identifier" 
}

variable "engine" {
  type        = string
  description = "The database engine to use"

}

variable "engine_version" {
  type        = string
  description = "The engine version to use"

}

variable "instance_class" {
  type        = string
  description = "The instance type of the RDS instance	"

}

variable "name" {
  type        = string
  description = "The DB name to create. If omitted, no database is created initially" 
}

variable "username" {
  type        = string
  description = "Username for the master DB user" 
}

variable "password" {
  type        = string
  description = "Password for the master DB user" 
}

variable "port" {
  type        = string
  description = "The port on which the DB accepts connections" 
}

variable "iam_database_authentication_enabled" {
  type        = bool
  description = "Specifies whether or not the mappings of AWS Identity and Access Management (IAM) accounts to database accounts are enabled" 
}

variable "vpc_security_group_ids" {
  type        = list(string)
  description = "List of VPC security groups to associate" 
}

variable "monitoring_role_name" {
  type        = string
  description = "Name of the IAM role which will be created when create_monitoring_role is enabled" 
}

variable "create_monitoring_role" {
  type        = bool
  description = "Create IAM role with a defined name that permits RDS to send enhanced monitoring metrics to CloudWatch Logs" 
}

variable "tags" {
  type        = map(string)	
  description = "A mapping of tags to assign to all resources"
}

variable "subnet_ids" {
  type        = list(string)
  description = "A list of VPC subnet IDs" 
}

variable "family" {
  type        = string
  description = "The family of the DB parameter group" 
}

variable "deletion_protection" {
  type        = bool
  description = "The database can't be deleted when this value is set to true" 
}

variable "major_engine_version" {
  type        = string
  description = "Specifies the major version of the engine that this option group should be associated with" 
}

variable "allocated_storage" {
  type        = string
  description = "The allocated storage in gigabytes" 
}