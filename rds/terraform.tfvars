tags                    = {
    Owner       = "user"
    Environment = "dev"
  }
create_monitoring_role              = true
subnet_ids                          = ["subnet-0c9b5298bf62c0a3a", "subnet-02b9bcfe64918fafb"]
family                              = "mysql5.7"
deletion_protection                 = false
identifier                          = "testdbyuliia"
engine                              = "mysql"
engine_version                      = "5.7.19"
instance_class                      = "db.t2.large"
username                            = "yuliiatest"
password                            = "YourPwdShouldBeLongAndSecure!"
port                                = "3306"
iam_database_authentication_enabled = true
vpc_security_group_ids              = ["sg-060a2ac72a63410a9"]
monitoring_role_name                = "MyRDSMonitoringRole"
name                                = "testdbyuliia"
major_engine_version                = "5.7"
allocated_storage                   = 5
region                              = "eu-central-1"
