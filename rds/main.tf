#Root module calls these modules which can also be used separately to create independent resources:
#db_instance - creates RDS DB instance
#db_subnet_group - creates RDS DB subnet group
#db_parameter_group - creates RDS DB parameter group
#db_option_group - creates RDS DB option group

module "db" {
  source                              = "terraform-aws-modules/rds/aws"
  version                             = "~> 3.0"
  identifier                          = var.identifier
  engine                              = var.engine
  engine_version                      = var.engine_version
  instance_class                      = var.instance_class
  name                                = var.name
  username                            = var.username
  password                            = var.password
  port                                = var.port
  iam_database_authentication_enabled = var.iam_database_authentication_enabled
  vpc_security_group_ids              = var.vpc_security_group_ids
  monitoring_role_name                = var.monitoring_role_name
  create_monitoring_role              = var.create_monitoring_role
  tags                                = var.tags
  subnet_ids                          = var.subnet_ids
  family                              = var.family
  deletion_protection                 = var.deletion_protection
  major_engine_version                = var.major_engine_version
  allocated_storage                   = var.allocated_storage


}