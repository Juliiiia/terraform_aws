module "elb_http" {
  source                = "terraform-aws-modules/elb/aws"
  version               = "~> 2.0"
  name                  = var.name
  subnets               = var.subnets
  security_groups       = var.security_groups
  internal              = var.internal
  listener              = var.listener
  health_check          = var.health_check
  // ELB attachments
  number_of_instances   = var.number_of_instances
  instances             = var.instances

}