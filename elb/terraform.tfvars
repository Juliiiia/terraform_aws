  name            = "elb-testyuliia"
  subnets         = ["subnet-02b9bcfe64918fafb", "subnet-065d9f19bd834c0cc" ]
  security_groups = ["sg-060a2ac72a63410a9"]
  internal        = false
  listener = [
    {
      instance_port     = 80
      instance_protocol = "HTTP"
      lb_port           = 80
      lb_protocol       = "HTTP"
    },
    {
      instance_port     = 8080
      instance_protocol = "http"
      lb_port           = 8080
      lb_protocol       = "http"
    },
  ]
  health_check = {
    target              = "HTTP:80/"
    interval            = 30
    healthy_threshold   = 2
    unhealthy_threshold = 2
    timeout             = 5
  }
  // ELB attachments
  number_of_instances = 2
  instances           = ["i-0f0d5f6d732815ef1", "i-0f653e030b9c8bb24"]
  region = "eu-central-1"
