variable "name" {
  type        = string
  description = "Name elb" 
}

variable "subnets" {
  type        = list(string)
  description = "A list of subnet IDs to attach to the ELB" 
}

variable "security_groups" {
  type        = list(string)
  description = "A list of security group IDs to assign to the ELB" 
}

variable "internal" {
  type        = bool
  description = "If true, ELB will be an internal ELB(true/false)" 
}

variable "listener" {
  type        = list(map(string))
  description = "A list of listener blocks" 
}

variable "health_check" {
  type        = map(string)
  description = "A health check block" 
}

variable "number_of_instances" {
  type        = number
  description = "Number of instances to attach to ELB" 
}

variable "instances" {
  type        = list(string)
  description = "List of instances ID to place in the ELB pool" 
}