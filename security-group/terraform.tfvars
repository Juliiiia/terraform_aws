  sg_name                  = "yuliiatestsgg"
  description              = "Test sg"
  vpc_id                   = "vpc-0657f4cf242b8542b"
  ingress_cidr_blocks      = ["85.223.209.18/32"]
  ingress_rules            = ["https-443-tcp"]
  ingress_with_cidr_blocks = [
    {
      from_port   = 8080
      to_port     = 8090
      protocol    = "tcp"
      description = "User-service ports"
      cidr_blocks = "10.10.0.0/16"
    },
    {
      rule        = "postgresql-tcp"
      cidr_blocks = "0.0.0.0/0"
    },
  ]
    region = "eu-central-1"
