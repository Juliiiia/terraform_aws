
variable "sg_name" {
  type        = string
  description = "Name SG" 
}

variable "description" {
  type        = string
  description = "Description" 
}

variable "vpc_id" {
  type        = string
  description = "ID of the VPC where to create security group" 
}

variable "ingress_cidr_blocks" {
  type        = list(string)
  description = "List of IPv4 CIDR ranges to use on all ingress rules" 
}

variable "ingress_rules" {
  type        = list(string)
  description = "List of ingress rules to create by name" 
}

variable "ingress_with_cidr_blocks" {
  type        = list(map(string))
  description = "List of ingress rules to create where 'cidr_blocks' is used" 

}
