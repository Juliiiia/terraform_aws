

variable "name_vpc" {
  type        = string
  description = "Name vpc" 
}

variable "cidr" {
  type        = string
  description = "The CIDR block for the VPC" 
}

variable "azs" {
  type        = list(string)	
  description = "A list of availability zones names or ids in the region" 
}

variable "private_subnets" {
  type        = list(string)	
  description = "A list of private subnets inside the VPC" 
}

variable "public_subnets" {
  type        = list(string)	
  description = "A list of public subnets inside the VPC" 
}

variable "enable_nat_gateway" {
  type        = bool
  description = "Should be true if you want to provision NAT Gateways for each of your private networks" 
}

variable "enable_vpn_gateway" {
  type        = bool
  description = "Should be true if you want to create a new VPN Gateway resource and attach it to the VPC" 
}
