data "aws_eks_cluster" "cluster" {
  name                   = module.cluster.cluster_id
}
//Get an authentication token to communicate with an EKS cluster.
data "aws_eks_cluster_auth" "cluster" {
  name                   = module.cluster.cluster_id
}

provider "kubernetes" {
  host                   = data.aws_eks_cluster.cluster.endpoint
  cluster_ca_certificate = base64decode(data.aws_eks_cluster.cluster.certificate_authority.0.data)
  token                  = data.aws_eks_cluster_auth.cluster.token
  load_config_file       = false
  version                = "~> 1.9"
}

module "cluster" {
  source                 = "terraform-aws-modules/eks/aws"
  cluster_name           = var.cluster_name
  cluster_version        = var.cluster_version
  subnets                = var.subnets
  vpc_id                 = var.vpc_id
  worker_groups          = var.worker_groups
}