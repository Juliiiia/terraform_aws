variable "cluster_name" {
  type        = string
  description = "Cluster name" 
}

variable "cluster_version" {
  type        = string
  description = "Kubernetes version to use for the EKS cluster"
  default     = "1.18" 
}

variable "subnets" {
  type        = list(string)
  description = "A list of subnets to place the EKS cluster and workers within"
}

variable "vpc_id" {
  type        = string
  description = "VPC where the cluster and workers will be deployed" 
}

variable "worker_groups" {
  type        = any
  description = "Instance type for worker groups" 
}


