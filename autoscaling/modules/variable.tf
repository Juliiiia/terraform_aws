variable "sg_name" {
  type        = string
  description = "SG name" 
}

variable "lt_name" {
  type        = string
  description = "LT name" 
}
variable "min_size" {
  type        = number
  description = "The minimum size of the autoscaling group" 
}

variable "max_size" {
  type        = number
  description = "The maximum size of the autoscaling group" 
}

variable "desired_capacity" {
  type        = number
  description = "The number of Amazon EC2 instances that should be running in the autoscaling group" 
}

variable "wait_for_capacity_timeout" {
  type        = number
  description = "A maximum duration that Terraform should wait for ASG instances to be healthy before timing out. (See also Waiting for Capacity below.) Setting this to '0' causes Terraform to skip all Capacity Waiting behavior" 
}

variable "health_check_type" {
  type        = string
  description = "EC2 or ELB. Controls how health checking is done" 
}


variable "vpc_zone_identifier" {
  type        = list(string)
  description = "A list of subnet IDs to launch resources in. Subnets automatically determine which availability zones the group will reside. Conflicts with availability_zones" 
}

variable "instance_refresh" {
  type        = any
  description = "If this block is configured, start an Instance Refresh when this Auto Scaling Group is updated" 
}

variable "launch_template_name" {
  type        = string
  description = "Name of launch template to be created" 
}

variable "description" {
  type        = string
  description = "Description" 
}

variable "update_default_version" {
  type        = bool
  description = "Whether to update Default Version each update(true/false)" 
}

variable "use_lt" {
  type        = bool
  description = "Determines whether to use a launch template in the autoscaling group or not(true/false)" 
}

variable "create_lt" {
  type        = bool
  description = "Determines whether to create launch template or not(true/false)" 
}

variable "image_id" {
  type        = string
  description = "The AMI from which to launch the instance" 
}

variable "instance_type" {
  type        = string
  description = "The type of the instance to launch" 
}

variable "ebs_optimized" {
  type        = bool
  description = "If true, the launched EC2 instance will be EBS-optimized(true/false)" 
}

variable "enable_monitoring" {
  type        = bool
  description = "Enables/disables detailed monitoring(true/false)" 
}

variable "cpu_options" {
  type        = map(string)
  description = "The CPU options for the instance" 
}

variable "credit_specification" {
  type        = map(string)
  description = "Customize the credit specification of the instance" 
}

variable "instance_market_options" {
  type        = any
  description = "The market (purchasing) option for the instance" 
}

variable "placement" {
  type        = map(string)
  description = "The placement of the instance" 
}

variable "tags" {
  type        = list(map(string))
  description = "A list of tag blocks. Each element should have keys named key, value, and propagate_at_launch" 
}

variable "block_device_mappings" {
  type        = list(any)
  description = "Specify volumes to attach to the instance besides the volumes specified by the AMI" 
}