module "asg" {
  source                    = "terraform-aws-modules/autoscaling/aws"
  version                   = "~> 4.0"
  name                      = var.sg_name
  min_size                  = var.min_size
  max_size                  = var.max_size
  desired_capacity          = var.desired_capacity
  wait_for_capacity_timeout = var.wait_for_capacity_timeout
  health_check_type         = var.health_check_type
  vpc_zone_identifier       = var.vpc_zone_identifier
  instance_refresh          = var.instance_refresh
  lt_name                   = var.lt_name
  description               = var.description
  update_default_version    = var.update_default_version
  use_lt                    = var.use_lt
  create_lt                 = var.create_lt
  image_id                  = var.image_id
  instance_type             = var.instance_type
  ebs_optimized             = var.ebs_optimized
  enable_monitoring         = var.enable_monitoring
  block_device_mappings     = var.block_device_mappings
  cpu_options               = var.cpu_options
  credit_specification      = var.credit_specification
  instance_market_options   = var.instance_market_options
  placement                 = var.placement
  tags                      = var.tags
}