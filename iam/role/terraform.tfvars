  trusted_role_arns = [
    "arn:aws:iam::958173500045:root"
      ]
  create_admin_role         = true

  create_poweruser_role      = true
  poweruser_role_name        = "Testyuliia"

  create_readonly_role       = true
  readonly_role_requires_mfa = false
  region                     = "eu-central-1"
