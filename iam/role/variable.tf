variable "trusted_role_arns" {
  type        = list(string)
  description = "Trusted role arns" 
}

variable "create_admin_role" {
  type        = bool
  description = "Create admin role(true/false)" 
}

variable "create_poweruser_role" {
  type        = bool
  description = "Create poweruser role(true/false)" 
}

variable "poweruser_role_name" {
  type        = string
  description = "Role name" 
}

variable "create_readonly_role" {
  type        = bool
  description = "create readonly role(true/false)" 
}

variable "readonly_role_requires_mfa" {
  type        = bool
  description = "Readonly role requires mfa(true/false)" 
}
