#https://github.com/terraform-aws-modules/terraform-aws-iam/blob/master/modules/iam-assumable-roles/main.tf
module "iam_role" {
  source                    = "terraform-aws-modules/iam/aws//modules/iam-assumable-roles"
  version                   = "~> 3.0"
  trusted_role_arns         = var.trusted_role_arns
  create_admin_role         = var.create_admin_role

  create_poweruser_role      = var.create_poweruser_role
  poweruser_role_name        = var.poweruser_role_name

  create_readonly_role       = var.create_readonly_role
  readonly_role_requires_mfa = var.readonly_role_requires_mfa
}