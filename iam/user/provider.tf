provider "aws" {
  region  = var.region
  profile = "dev2"
}
variable "region" {
  description = "The AWS region to deploy"
  type        = string

}