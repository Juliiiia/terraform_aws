
variable "name" {
  type        = string
  description = "Username" 
}

variable "pgp_key" {
  type        = string
  description = "PGP key used to encrypt sensitive data for this user (if empty - secrets are not encrypted)" 
}

variable "password_reset_required" {
  type        = bool
  description = "Password reset required" 
}

variable "upload_iam_user_ssh_key" {
  type        = bool
  description = "Upload iam user ssh key" 
}

variable "force_destroy" {
  type        = bool
  description = "Force destroy user" 
}

variable "ssh_public_key" {
  type        = string
  description = "Ssh public key" 
}