module "iam_user" {
  source                  = "terraform-aws-modules/iam/aws//modules/iam-user"
  version                 = "~> 3.0"

  name                    = var.name
  force_destroy           = true
  pgp_key                 = var.pgp_key
  password_reset_required = var.password_reset_required
  upload_iam_user_ssh_key = var.upload_iam_user_ssh_key
  ssh_public_key          = var.ssh_public_key      
}