module "iam_policy" {
  source  = "terraform-aws-modules/iam/aws//modules/iam-policy"
  version = "~> 3.0"

  name        = var.name
  path        = var.path
  description = var.description

  policy = file("./policy.tpl")
}