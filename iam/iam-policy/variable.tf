variable "name" {
  type        = string
  description = "Name of IAM policy and IAM group" 
}

variable "path" {
  type        = string
  description = "Path" 
}

variable "description" {
  type        = string
  description = "Description" 
}
